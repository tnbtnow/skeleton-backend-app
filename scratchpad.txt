M5 org.springframework.security.core.userdetails.UsersDetailsRepository
public interface UserDetailsRepository {
	Mono<UserDetails> findByUsername(String username);
}
org.springframework.security.core.userdetails.MapUserDetailsRepository implements UsersDetailsRepository


M5+RC1 org.springframework.security.core.userdetails.UserDetailsService {
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
}

RC1 public interface ReactiveUserDetailsService {

    	Mono<UserDetails> findByUsername(String username);
    }
RC1 public class MapReactiveUserDetailsService implements ReactiveUserDetailsService

spring-security-config-RC1 release date: 30 oct (M5: 09 Oct)
spring-security-webflux-5.0.0.BUILD-20171018.203026-35.pom release date: 18 oct. (M5 released 09-Oct)

spring-security-config-RC1 looks older (less reactive) than spring-security-webflux-5.0.0.BUILD-20171018